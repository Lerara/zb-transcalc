<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'css/zoom.menu.css',
        //'css/content.css',
    ];
    public $js = [
        'web/js/zoom.menu-min.js',
        'web/js/less.js',
        'web/js/page.js',
        'web/js/prices.js'
    ];
    public $depends = [
        //'yii\web\YiiAsset',
    ];
}
