<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\Account;

Yii::$app->view->params['start'] = 'none';

class SiteController extends Controller
{

    public $start = "none";

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        //$this->start = "start";
        $this->view->params['start'] = 'start';
        $this->layout = "main";
        return $this->render('index');
    }

    public function actionContact()
    {
        return $this->render('contact');
    }

    public function actionConditions()
    {
        return $this->render('conditions');
    }

    public function actionImprint()
    {
        return $this->render('imprint');
    }

    public function actionPrice()
    {
        return $this->render('price');
    }

    public function actionPrivacypolicy()
    {
        return $this->render('privacypolicy');
    }

    public function actionRegistration()
    {
        return $this->render('registration');
    }

     public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $query = Account::find();
            $accounts = $query->orderBy('firstname')->all();
            return $this->render('../account/new', [
                'accounts' => $accounts,
            ]);
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
