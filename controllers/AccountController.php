<?php
namespace app\controllers;

use yii\web\Controller;
use app\models\Account;
use yii;

Yii::$app->view->params['start'] = 'none';

class AccountController extends Controller
{
    public function actionNew()
    {
        $query = Account::find();


        $accounts = $query->orderBy('firstname')->all();


       return $this->render('new', [
            'accounts' => $accounts,
        ]);
    }
}  