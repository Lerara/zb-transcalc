<?php
namespace app\models;


use app\models\Account;

class User extends \yii\base\Object implements \yii\web\IdentityInterface
{
    public $id;
    public $createdAt;
    public $updatedAt;
    public $deleted;
    public $firstname;
    public $lastname;
    public $password;
    public $title;
    public $phone;
    public $mobile;
    public $fax;
    public $email;
    public $gender;
    public $role;
    public $companyId;
    public $authKey;
    public $accessToken;


    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return Account::findOne($id) !== null ? new static(Account::findOne($id)) : null;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (Account::find()->all() as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param  string      $username
     * @return static|null
     */
    public static function findByEmail($email)
    {
        $user = Account::findOne(['email' => $email,]);

        if ($user !== null){
            return new static($user);
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }
}