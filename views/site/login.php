<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h2>Betrete deinen Arbeitsplatz.</h2>

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'options' => ['class' => 'login-form'],
        'fieldConfig' => [
            'template' => "{input}",
        ],
    ]); ?>

        <?= $form->field($model, 'email',['inputOptions' => ['placeholder' => 'Email',],]);?>

        <?= $form->field($model, 'password',['inputOptions' => ['placeholder' => 'Passwort',],])->passwordInput(); ?>
        <a class="password-forget" href="">Passwort vergessen?</a>

        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-11">
                <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
        </div>

        <a class="new-account" href="<?= Url::to(['site/registration']);?>">Du hast noch kein Konto?</a>

    <?php ActiveForm::end(); ?>
</div>
