<?php
/* @var $this yii\web\View */
$this->title = 'Startseite | TransClac';
?>


<div id="scroll-to-about" class="about">
    <div class="about-what">
        <div class="about-img"><img src="web/images/transcalc-imac-what-it-is.jpg" ></div>
        <div class="about-text-wrapper">
            <div class="about-text">
                <h3>Konkurrenzfähig durch richtige Klalkulation</h3>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nec libero ex. Donec auctor lobortis orci in semper. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Phasellus ex risus, interdum ut feugiat non, sollicitudin ac dui. In purus ipsum, sollicitudin tincidunt ornare ac, fringilla vitae eros. Curabitur hendrerit aliquam ipsum nec pretium. Proin at tortor id lacus tincidunt eleifend.
                </p>  
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="about-sections">
        <h3>Konkurrenzfähig durch richtige Klalkulation</h3>
        <div class="about-section right text-middle-right">
            <div class="about-section-img"><img src="web/images/transcalc-packages.jpg" ></div>
            <div class="about-section-text">
              <div class="about-section-text-content">
                <p>
                   Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nec libero ex. Donec auctor lobortis orci in semper. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Phasellus ex risus, interdum ut feugiat non, sollicitudin ac dui. In purus ipsum, sollicitudin tincidunt ornare ac, fringilla vitae eros. Curabitur hendrerit aliquam ipsum nec pretium. Proin at tortor id lacus tincidunt eleifend.
                </p>
              </div>
            </div>  
            <div class="clear"></div>         
        </div>
        <div class="about-section left img-middle-right">
            <div class="about-section-img"><img src="web/images/transcalc-transporter.jpg" ></div>
            <div class="about-section-text">
              <div class="about-section-text-content">
                <p >
                   In fringilla convallis diam non interdum. Vestibulum commodo libero non justo eleifend ornare. Vestibulum posuere erat congue justo suscipit pharetra sit amet a libero. Quisque sit amet tincidunt tellus. Ut quis nunc sed erat suscipit tristique eget eget enim. Sed tempus laoreet porta. Vestibulum vestibulum ligula in lectus condimentum, et scelerisque mauris elementum. Sed facilisis lorem ac nisi aliquam tempus a in leo. Aenean id libero gravida, convallis tortor placerat, blandit magna.
                </p>
              </div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="about-section right img-middle-left">
            <div class="about-section-img"><img src="web/images/transcalc-calculator.jpg"></div>
            <div class="about-section-text">
              <div class="about-section-text-content">
                <p>
                    Sed convallis mauris massa, cursus laoreet ante elementum suscipit. Etiam eleifend risus elit, sed pretium nisi semper et. Pellentesque ipsum ante, mollis eu libero et, viverra interdum metus. Suspendisse ipsum neque, volutpat eget elementum non, rutrum sed sapien. Proin consequat metus eu neque tincidunt, nec egestas velit convallis. Praesent vestibulum dui a purus aliquet tristique. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur ullamcorper, ligula in porttitor aliquam, mi felis viverra leo, ut varius eros tellus eu orci. Praesent feugiat eu orci vel gravida. Fusce vel justo tincidunt, vestibulum nibh vitae, sagittis arcu. Morbi feugiat, erat nec consequat sodales, nunc felis venenatis nunc, id vestibulum ex mi vitae urna. Praesent vestibulum ipsum ut erat ultricies, in rhoncus felis volutpat. Fusce nibh ipsum, finibus in velit egestas, sodales sodales massa. Nunc sed magna quis tortor ullamcorper scelerisque. Donec finibus ex vitae urna convallis semper.
                </p>
              </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>


<div class="header-wrapper">
<div class="plan-header">
      <h2>Wähle deinen Plan!</h2>
      <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.  </p>
    </div>
    <div class="plan-buttons">
      <div class="btn-group" id="button-wrapper">
        <a class="button selected" id="kostenlos" onclick="contentScroll(0)">Kostenlos</a><a class="button" id="proPlus" onclick="contentScroll(1)">Pro plus</a><a class="button" id="pro" onclick="contentScroll(2)">Pro</a>
      </div>
    </div>
    </div>
<div id="scroll-to-unsere-plane" class="unsere-plane">
    
    <div class="plan-super-wrapper" id="wrapper">
      <div class="plan-wrapper-kostenlos" id="kostenlosWrapper">
        <div class="upper-content">
          <div class="price">0,-</div>
          <div>
          <p>
            <h1 class="money">EUR</h1>
            <p>pro Monat</p>
            </p>
          </div>
        </div>
        <div class="middle-content">
          <ul>
            <li>Lorem ipsum dolor sit amet</li>
            <li>Lorem ipsum dolor sit amet</li>
            <li>Lorem ipsum dolor sit amet</li>
            <li>Lorem ipsum dolor sit amet</li>
            <li>Lorem ipsum dolor sit amet</li>
          </ul>
        </div>
        <div class="bottom-content">
          <button class="plan-content-button">jetzt Starten</button>
        </div>
      </div>
      <div class="plan-wrapper-pro-plus" id="proPlusWrapper">
        <div class="upper-content">
          <div class="price">99,-</div>
          <div>
            <h2 class="money">EUR</h2>
            <p>pro Monat</p>
          </div>
        </div>
        <div class="middle-content">
          <ul>
            <li>Lorem ipsum dolor sit amet</li>
            <li>Lorem ipsum dolor sit amet</li>
            <li>Lorem ipsum dolor sit amet</li>
            <li>Lorem ipsum dolor sit amet</li>
            <li>Lorem ipsum dolor sit amet</li>
            <li>Lorem ipsum dolor sit amet</li>
            <li>Lorem ipsum dolor sit amet</li>
            <li>Lorem ipsum dolor sit amet</li>
            <li>Lorem ipsum dolor sit amet</li>
          </ul>
        </div>
        <div class="bottom-content">
          <button class="plan-content-button">jetzt Starten</button>
        </div>
      </div>
      <div class="plan-wrapper-pro" id="proWrapper">
        <div class="upper-content">
          <div class="price">39,-</div>
          <div>
            <div class="money">EUR</div>
            <p>pro Monat</p>
          </div>
        </div>
        <div class="middle-content">
          <ul>
            <li>Lorem ipsum dolor sit amet</li>
            <li>Lorem ipsum dolor sit amet</li>
            <li>Lorem ipsum dolor sit amet</li>
            <li>Lorem ipsum dolor sit amet</li>
            <li>Lorem ipsum dolor sit amet</li>
            <li>Lorem ipsum dolor sit amet</li>
            <li>Lorem ipsum dolor sit amet</li>
          </ul>
        </div>
        <div class="bottom-content">
          <button class="plan-content-button">jetzt Starten</button>
        </div>
      </div>
    </div>
    <div style="clear:both;"></div>
</div>