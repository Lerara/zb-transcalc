<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
$this->title = 'New | TransClac';
?>
<h1>Accounts</h1>
<ul>
<?php foreach ($accounts as $account): ?>
    <li>
        <?= Html::encode("{$account->firstname}") ?>
    </li>
<?php endforeach; ?>
</ul>