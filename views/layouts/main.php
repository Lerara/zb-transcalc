<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\widgets\Menu;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="web/js/zoom.menu-min.js"></script>
    <link rel="stylesheet/less" type="text/css" href="../../web/css/main.less" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="content-wrapper">
    <div id="my-zoom-menu" class="zm-animation-wrapper zm-closed">
        <div class="zm-menu">
             <?php
                echo Menu::widget([
                    'options' => ['class' => 'zb-menu-list'],
                    'items' => [
                        ['label' => 'TransClac', 'url' => ['/site/index']],
                        ['label' => 'Preise', 'url' => ['/site/price']],
                        ['label' => 'Kontakt', 'url' => ['/site/contact']],
                        ['label' => 'Anmelden', 'url' => ['/site/registration']],
                        ['label' => 'AGBs', 'url' => ['/site/conditions']],
                        ['label' => 'Impressum', 'url' => ['/site/imprint']],
                        Yii::$app->user->isGuest ?
                            ['label' => 'Login', 'url' => ['/site/login']] :
                            [
                                'label' => 'Logout ',
                                'url' => ['/site/logout'],
                                'linkOptions' => ['data-method' => 'post']
                            ],
                                ],
                            ]);
            ?>
        </div>
        <div class="menu-button" id="menu-button" onclick="zoomMenu.show();">
            <div class="nv-line"></div>
            <div class="nv-line"></div>
            <div class="nv-line"></div>
        </div>
        <div class="zm-content-wrapper">
            <div class="zm-content" id="zm-content">
                <div class="head-line <?php if ($this->params['start'] !=='start') echo 'inAppHead' ?>" id="header" align="center">
                         <img src="web/images/transcalc-logo.png"  >
                         <h2 align="center">Mit geringnem Aufwand <br><b>Geld gespart!</b></br></h2>
                </div>
                <div class="menu <?php if ($this->params['start'] !=='start') echo 'inAppMenu' ?>" id="menu">
                    <div class="menu-logo"><img src="web/images/transcalc-logo.png"></div>
                    <?php
                        echo Menu::widget([
                            'options' => ['class' => 'menu-list'],
                            'items' => [
                                ['label' => 'TransClac', 'url' => ['/site/index']],
                                ['label' => 'Preise', 'url' => ['/site/price']],
                                ['label' => 'Kontakt', 'url' => ['/site/contact']],
                                ['label' => 'Anmelden', 'url' => ['/site/registration']],
                            ],
                        ]);
                    ?>
                    <div class="clear"></div>
                </div>
                <div id="site-content">
                    <?= $content ?>
                </div>
                <footer class="footer">
                    <?php
                        echo Menu::widget([
                            'options' => ['class' => 'zb-menu-list'],
                            'items' => [
                                ['label' => 'Anmeldung', 'url' => ['/site/registration']],
                                ['label' => 'Datenschutzerklärung', 'url' => ['/site/privacypolicy']],
                                ['label' => 'Impressum', 'url' => ['/site/imprint']],
                                ['label' => 'AGBs', 'url' => ['/site/conditions']],
                                ['label' => 'Kontakt', 'url' => ['/site/contact']],
                            ],
                        ]);
                    ?>
                    <img src="web/images/transcalc-logo-small.png">
                    <p class="copyright">&copy; <?= date('Y') ?> Copyright Transcalc GmbH </p>
                </footer>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var zoomMenu = new ZMZoomMenu('my-zoom-menu');
</script>
<script>
  less = {
    env: "development"
  };
</script>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
