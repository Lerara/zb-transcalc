window.onscroll = function() {ScrollFunction();};


function ScrollFunction()  {

	var menu = document.getElementById('menu');
	var site = document.getElementById('site-content');
	var header = document.getElementById('header');

	if(header.style.display == "none") header.style.height = "0px";

	if (window.pageYOffset !== undefined) 
	{
		var pos = window.pageYOffset;
	}
	else
	{
		var pos = document.documentElement.scrollTop;
	}
	if (pos > header.offsetHeight)
	{
		menu.style.position = "fixed";
		menu.style.top = '0px';
		site.style.marginTop = menu.offsetHeight+'px';
	}
	else
	{
		menu.style.position = "static";
		site.style.marginTop = '0px';
	}
}